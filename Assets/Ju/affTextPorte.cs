﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class affTextPorte : MonoBehaviour
{
    public GameObject txtInfos;
    
    void Start()
    {
        txtInfos.SetActive(false);
    }
    
    void OnTriggerEnter(Collider ply)
    {
        if (ply.gameObject.tag == "ObjetManip")
        {
             txtInfos.SetActive(true);
        }
    }
        
    void OnTriggerExit(Collider ply)
    {
        if (ply.gameObject.tag == "ObjetManip")
        {
            txtInfos.SetActive(false);
        }
    }
}
