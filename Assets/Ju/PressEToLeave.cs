﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PressEToLeave : MonoBehaviour
{ 
    public string levelToLoad;

    void OnTriggerStay (Collider col)
    {
        if(col.gameObject.tag == "ObjetManip")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                SceneManager.LoadScene(levelToLoad);
            }
        }
    }
}