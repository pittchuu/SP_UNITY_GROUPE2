﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiensObjet : MonoBehaviour
{
private bool Prend = false; // faux tant qu'on a pas trouvé l'objet à prendre

public GameObject objetNom; // l'objet qu'on veut prendre


    void Update()
    {
    // prendre l'objet: touche E, Prend = vrai, objet existe
        if (Input.GetKeyDown(KeyCode.E) && Prend && objetNom != null)
        {
            objetNom.GetComponent<Rigidbody>().isKinematic = true; // pour que l'objet ne soit plus soumis à la gravité
            objetNom.transform.position = GameObject.Find("TiensObj").transform.position; // La position de l'objet prend celle de du gameobject qui porte le nom "tiensObjet"
            objetNom.gameObject.transform.parent = GameObject.Find("TiensObj").transform; // L'objet devient un enfant de "tiensObjet" afin de rester "collé" à "TiensObjet"
        }

        // Déposer l'objet
        if (Input.GetMouseButtonDown(0) && objetNom != null) // condition clique gauche souris et l'objet à saisir existe
        {
            objetNom.GetComponent<Rigidbody>().isKinematic = false; // l'objet est de nouveau soumis à la gravité donc tombe
            objetNom.gameObject.transform.parent = null;
        }
    }
    // évènement déclencheur le collider du joueur est entré dans le collider de l'objet qui porte le tag "objetManip"
    void OnTriggerEnter(Collider col)

    {
        if (col.gameObject.tag == "ObjetManip")
        {
            objetNom = col.gameObject;
            Prend = true;
        }
    }
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "ObjetManip")
        {
            Prend = false;
            objetNom = null;
        }
    }
}
