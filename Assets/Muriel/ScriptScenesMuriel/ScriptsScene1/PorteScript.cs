﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PorteScript : MonoBehaviour
{
    private AudioSource mySource;
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        mySource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

     void OnTriggerEnter(Collider other)
    {
        mySource.Stop();
        anim.enabled = true;
        anim.SetBool("S_Ouvre", true);
       
    }

    private void OnTriggerExit(Collider other)
    {
        anim.enabled = true;
        anim.SetBool("S_Ouvre", false);
        mySource.Play();
    }

    void pauseAnimationEvent()
    {
    anim.enabled = false;
 
    }

    void pauseAnimationEvent2()
    {
        anim.enabled = false;

    }
}
