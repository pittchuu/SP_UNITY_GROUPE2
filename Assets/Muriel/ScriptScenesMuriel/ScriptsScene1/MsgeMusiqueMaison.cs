﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MsgeMusiqueMaison : MonoBehaviour
{
    private AudioSource mySource;

    private void Start()
    {
        mySource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !mySource.isPlaying)
        {
            mySource.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && mySource.isPlaying)
        {
            mySource.Stop();
        }
    }
}
