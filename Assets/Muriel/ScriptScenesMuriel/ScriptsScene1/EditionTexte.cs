﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class EditionTexte : MonoBehaviour 
{  // script texte portail quantique
    public Text txtInfos;
    public string levelToLoad;
    

   

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "ObjetManip")
        {
            txtInfos.text = "Clique sur P pour ouvrir le portail quantique";
           if(Input.GetButtonDown("Use"))
            {
                PlayerPrefs.DeleteKey("ReboursReload");
                SceneManager.LoadScene(levelToLoad);
               
            }
        }
    }
    void OnTriggerExit(Collider ply)
    {
        if (ply.gameObject.tag == "ObjetManip")
        {
            txtInfos.text = "";
        }
    }

}
