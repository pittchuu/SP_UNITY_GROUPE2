﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotationTourCanon : MonoBehaviour
{
    

    public float range = 15f; //zone de détection maximale de la cible par la tourelle

    public string plyTag = "Player"; //la cible est ici le joueur

    public Transform partieRotate; //var stockant la partie rotative de la tourelle

    public float fireRate = 1f; //cadence de tir = 1 signifie une balle par seconde

    

    public GameObject projectilePrefab;
    public Transform firePoint; // point de génération des projectiles

    
    private Transform cible; // la cible vers laquelle pointera la tourelle
    private float fireCountdown = 0f; // temps entre 2 tirs

    private AudioSource source;  // SON
   

    // private float turnSpeed= 0.1f; //vitesse de rotation de la tourelle utilisée dans la fonctionnalité Lerp



    void Awake() //SON
    {
        source = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update

    void Start()
    { // appel de façon répété d'une fonction mais indépendemment de la frame rate

        InvokeRepeating("UpdateTarget", 0f, 0.5f); //le 2ième paramètre concerne l'éventuel délai pour lancer la fonction, on met 0 car on veut chercher en permanence la cible, le 3ième paramètre =toutes les demi-secondes on appelle UpdateTarget  
    }

    //Récupération de l'ennemi, vérifier l'ennemi le plus proche, et s'il est dans le range, la tourelle tourne vers lui
    // création d'une fonction pour économiser ressources; cette fonction sera appelé x fois par secondes et non pas 60 fois si on a 60 frames par seconde avec la fonction Update
    void UpdateTarget()
    {
        GameObject ply = GameObject.FindGameObjectWithTag(plyTag);
        //si on plusieurs cibles on déclare un array GameObject[] cibles = même chose sauf qu'on utilise FindGameObjectWithTags

        float distancePlayer = Vector3.Distance(transform.position, ply.transform.position);
        if (ply != null && distancePlayer <= range)
        {
            cible = ply.transform;
        }
        else
        {
            cible = null;
        }

    }

    // Update is called once per frame

    // traite la partie rotation
    void Update()
    {
        if (cible == null)
        {
            return; //si la cible est nulle on ne lit pas la fonction Update
        }

        // on récupère la direction vers laquelle pointe la tourelle
        Vector3 dir = cible.position - transform.position; //pour savoir où doit pointer la tourelle
        Quaternion Lookrotation = Quaternion.LookRotation(dir);   //Crée une rotation pour pointer vers la cible; Quaternion sur Unity = gestion des rotations
        Vector3 rotation = Lookrotation.eulerAngles;  // convertit la rotation quaternion en rotation angle d'euler
        partieRotate.rotation = Quaternion.Euler(0f, rotation.y - 90, 0f);//on veut juste que la tourelle tourne autour de l'axe y

        if(fireCountdown <= 0f)
        {
            Shoot();  // appel d'une fonction tir qui effectue toutes les actions de tir
            fireCountdown = 1 / fireRate; // pouvoir ajouter un délai entre chaque tir
        }

        fireCountdown -= Time.deltaTime; //chaque seconde on enlève 1 seconde au délai restant
    }

    void Shoot()
    {
        // Debug.Log("Tir effectué"); // teste la fonction

               GameObject projectileGO = (GameObject)Instantiate(projectilePrefab, firePoint.position, firePoint.rotation); // on crée une variable locale. on fait apparaitre le projectile au point de génération
        Projectile projectile = projectileGO.GetComponent<Projectile>();//Projectile avec un P majuscule correspond à la classe Projectile donc au script Projectile

        if(projectile != null)// vérifie qu'on a pas oublié de mettre un script
        {
            projectile.Seek(cible);


        }
    }

    private void OnDrawGizmosSelected() //permet visualiser la portée de la tourelle dans scène
    {
        Gizmos.color = Color.yellow;//couleur du gizmo
        Gizmos.DrawWireSphere(transform.position, range);//affiche sur la scène une sphère autour de la tourelle visualisant l'espace où sera détecté la cible
    }
}
