﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CompteRebours : MonoBehaviour
{
    public GameObject afficheTexte;
    public int tempsRestant = 30;
    public bool retire = false;
    private bool activeCompteRebours = false;
    private Scene activeScene;


    void Start()
    {
        activeScene = SceneManager.GetActiveScene();

    }

    void Update()
    {
        if (activeCompteRebours == true)
        {
            afficheTexte.GetComponent<Text>().text = "00:" + tempsRestant;
            if (retire == false && tempsRestant > 0)
            {
                StartCoroutine(RetireTemps());
            }

            if (tempsRestant <= 0)
            {
                retire = true;
                StartCoroutine(RelanceScene());

            }
        }
    }
    IEnumerator RetireTemps()
    {
        retire = true;
        yield return new WaitForSeconds(1f);
        tempsRestant -= 1;
        if (tempsRestant < 10)
        {
            afficheTexte.GetComponent<Text>().text = "00:0" + tempsRestant;
        }
        else
        {
            afficheTexte.GetComponent<Text>().text = "00:" + tempsRestant;
        }
        retire = false;
    }

    IEnumerator RelanceScene()
    {

        afficheTexte.GetComponent<Text>().text = "Le compte à rebours est achevé !                Tu demeures dans cette boucle spatio-temporelle!";
        yield return new WaitForSeconds(2f);
        afficheTexte.gameObject.SetActive(false);
        SceneManager.LoadScene(activeScene.name);

    }

    private void OnTriggerEnter(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            activeCompteRebours = true;
        }

    }

}





