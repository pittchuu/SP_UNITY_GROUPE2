﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerText : MonoBehaviour
{
    public GameObject txtInfos;


    void Start()
    {
        txtInfos.SetActive(false);
    }


    void OnTriggerEnter(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            txtInfos.SetActive(true);
        }
    }

    void OnTriggerExit(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            txtInfos.SetActive(false);
        }
    }
}
