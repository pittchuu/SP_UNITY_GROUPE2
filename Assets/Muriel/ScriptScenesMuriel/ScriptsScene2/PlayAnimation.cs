﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimation : MonoBehaviour
{
    [SerializeField] private Animator myAnimationController;
    [SerializeField] private Animator myAnimationController2;
    [SerializeField] private Animator myAnimationController3;
    [SerializeField] private Animator myAnimationController4;
    [SerializeField] private Animator myAnimationController5;
    [SerializeField] private Animator myAnimationController6;
    [SerializeField] private Animator myAnimationController7;
    [SerializeField] private Animator myAnimationController8;
    [SerializeField] private Animator myAnimationController9;
    [SerializeField] private Animator myAnimationController10;
    private AudioSource music;


    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            myAnimationController.SetBool("Animation", false);
            myAnimationController2.SetBool("Animation", false);
            myAnimationController3.SetBool("Animation", false);
            myAnimationController4.SetBool("Animation", false);
            myAnimationController5.SetBool("Animation", false);
            myAnimationController6.SetBool("Animation", false);
            myAnimationController7.SetBool("Animation", false);
            myAnimationController8.SetBool("Animation", false);
            myAnimationController9.SetBool("Animation", false);
            myAnimationController10.SetBool("Animation", false);
           
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            myAnimationController.SetBool("Animation", true);
            myAnimationController2.SetBool("Animation", true);
            myAnimationController3.SetBool("Animation", true);
            myAnimationController4.SetBool("Animation", true);
            myAnimationController5.SetBool("Animation", true);
            myAnimationController6.SetBool("Animation", true);
            myAnimationController7.SetBool("Animation", true);
            myAnimationController8.SetBool("Animation", true);
            myAnimationController9.SetBool("Animation", true);
            myAnimationController10.SetBool("Animation", true);
        }
    }
}

