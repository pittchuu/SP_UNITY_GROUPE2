﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsigneKey : MonoBehaviour
{
    // Start is called before the first frame update
    public Text txtInfos;


    void Start()
    {
        
    }


    void OnTriggerEnter(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            txtInfos.text = "Clique sur E pour prendre la clé";
        }
    }

    void OnTriggerExit(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            txtInfos.text = "";
        }
    }
}
