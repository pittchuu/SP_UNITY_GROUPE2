﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CompteReboursArkanion : MonoBehaviour
{
    public GameObject afficheTexte;
    public int tempsRestant = 60;
    public bool retire = false;
    private bool activeCompteRebours = false;
    private Scene activeScene;
    private int tempsReload;




    void Start()
    {
        if (PlayerPrefs.HasKey("ReboursReload"))
        {
            tempsReload = PlayerPrefs.GetInt("ReboursReload");
            if (tempsReload > 0)
            {
                tempsRestant = tempsReload;
            }
        }

        activeScene = SceneManager.GetActiveScene();

    }

    void Update()
    {
        if (activeCompteRebours == true)
        {
            afficheTexte.GetComponent<Text>().text = "00:" + tempsRestant;
            if (retire == false && tempsRestant > 0)
            {
                StartCoroutine(RetireTemps());
            }

            if (tempsRestant <= 0)
            {
                retire = true;
                StartCoroutine(RelanceScene());

            }
        }
    }
    IEnumerator RetireTemps()
    {
        retire = true;
        yield return new WaitForSeconds(1f);
        tempsRestant -= 1;
        tempsReload = tempsRestant;
        PlayerPrefs.SetInt("ReboursReload", tempsReload);
        if (tempsRestant < 10)
        {
            afficheTexte.GetComponent<Text>().text = "00:0" + tempsRestant;
        }
        else
        {
            afficheTexte.GetComponent<Text>().text = "00:" + tempsRestant;
        }
        retire = false;
    }

    IEnumerator RelanceScene()
    {

        afficheTexte.GetComponent<Text>().text = "le compte à rebours est achevé";
        yield return new WaitForSeconds(2f);
        afficheTexte.gameObject.SetActive(false);
        SceneManager.LoadScene(activeScene.name);

    }

    private void OnTriggerEnter(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            activeCompteRebours = true;
        }

    }
    
}
