﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Transform cible; // position de la cible du projectile

    public float speed = 70f; //vitesse du projectile

    public GameObject effetImpact;


    public void Seek(Transform _cible) // création d'une fonction qui cherche la cible. On appellera cette fonction du script rotationTourCanon dans lequel on a aussi une variable cible qui référence le joueur. on appellera Seek en lui passant cette variable cible. il faut donc la déclarer en public 
    {
        cible = _cible;


    }

    // Update is called once per frame
    void Update()
    {
        if (cible == null)
        {
            Destroy(gameObject);  //on doit vérifier que la cible existe toujours
            return; //on arrête l'exécution du script
        }

        Vector3 dir = cible.position - transform.position; //même instruction que dans le script rotationTourCanon
        float distanceThisFrame = speed * Time.deltaTime;   //distance qu'on va parcourir pendant cette image


        if (dir.magnitude <= distanceThisFrame) //magnitude = longueur du vecteur dir donc distance entre la cible et l'emplacement de la balle
        {
            AtteintCible();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World); // dir.normalized renvoie un vecteur de magnitude 1 car sinon plus on se rapprochera de l'objet et moins vite on avancera. Permet de conserver la même vitesse
    }

    void AtteintCible()
    {
        GameObject effetInst = (GameObject)Instantiate(effetImpact, transform.position, transform.rotation); //on fait apparaitre les effets de particules
        Destroy(effetInst, 2f); // on fait disparaitre le PS de Hierarchy au bout de 2 secondes pour chaque projectile

        //Debug.Log("La balle a atteint sa cible");//verification du script
        Destroy(gameObject); // détruit le projectile au moment où il atteint la cible
    }
}
