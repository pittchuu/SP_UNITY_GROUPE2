﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PortailQuantique : MonoBehaviour
{
    // script texte portail quantique
    public Text txtInfos;
    public string levelToLoad;
    public GameObject musArkanion;
    

    private void Start()
    {
        musArkanion = GameObject.Find("MusiqueArkanion");
        
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "ObjetManip")
        {
            txtInfos.text = "Clique sur P pour ouvrir le portail quantique";
            if (Input.GetButtonDown("Use"))
            {
                PlayerPrefs.DeleteKey("ReboursReload");
                musArkanion.SetActive(false);
                SceneManager.LoadScene(levelToLoad);
               
               
                

            }
        }
    }
    void OnTriggerExit(Collider ply)
    {
        if (ply.gameObject.tag == "ObjetManip")
        {
            txtInfos.text = "";
        }
    }

}
