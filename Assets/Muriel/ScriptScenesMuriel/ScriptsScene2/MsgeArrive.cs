﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MsgeArrive : MonoBehaviour
{
    public GameObject textInfo1;
    public GameObject textInfo2;
    public GameObject textInfo3;
    private bool info1 = false;
    private bool info2 = false;
    private bool info3 = false;

    // Start is called before the first frame update
    void Start()
    {
        textInfo1.SetActive(false);
        textInfo2.SetActive(false);
        textInfo3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (info1 == false && info2 == false && info3 == false)
        {

            StartCoroutine(Info1());
        }

        if (info1 == true && info2 == false && info3 == false)
        {

            StartCoroutine(Info2());
        }

        if (info1 == true && info2 == true && info3 == false)
        {

            StartCoroutine(Info3());
        }

        if (info1 == true && info2 == false && info3 == true)
        {

            StartCoroutine(FinInfo());
        }




    }

    IEnumerator Info1()
    {
        textInfo1.SetActive(true);
        yield return new WaitForSeconds(1f);
        info1 = true;

    }

    IEnumerator Info2()
    {
        textInfo2.SetActive(true);
        yield return new WaitForSeconds(1f);
        info1 = true;
        info2 = true;

    }

    IEnumerator Info3()
    {
        textInfo2.SetActive(false);
        yield return new WaitForSeconds(1f);
        textInfo3.SetActive(true);
        yield return new WaitForSeconds(2f);
        info2 = false;
        info3 = true;

    }

    IEnumerator FinInfo()
    {
        textInfo1.SetActive(false);
        yield return new WaitForSeconds(1f);
        textInfo3.SetActive(false);


    }

}
