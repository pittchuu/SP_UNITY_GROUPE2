﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinSceneFred : MonoBehaviour
{
    // Start is called before the first frame update
    public string levelToLoad;

    void OnTriggerStay(Collider other)
    {
        if (GameObject.Find("FPSPlayer").GetComponent<ScoreScript>().score ==3)
        {
            if (Input.GetKeyDown(KeyCode.E)) {
                SceneManager.LoadScene(levelToLoad);
            }
            
        }
    }
}