﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Poteau : MonoBehaviour
{

    private Text TxtInfos;
    // Start is called before the first frame update
    void Start()
    {
        TxtInfos = GameObject.Find("TextPoto").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            TxtInfos.text = "Votre objectif ? Sortir de ce labyrinthe... Pour le moment :)";
        }
    }

    private void OnTriggerExit(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            TxtInfos.text = "";
        }
    }
}
