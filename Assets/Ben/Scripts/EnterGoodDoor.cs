﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnterGoodDoor : MonoBehaviour
{

    private Text TxtD5;
    private bool ZoneD5 = false;
    // Start is called before the first frame update
    void Start()
    {
        TxtD5 = GameObject.Find("TextD5").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
         if(Input.GetKeyDown(KeyCode.E) && ZoneD5) {
            SceneManager.LoadScene("DemoScene");
        }
    }

    private void OnTriggerEnter(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            TxtD5.text = "Aucune des portes à gauche";
            ZoneD5 = true;
        }
    }

    private void OnTriggerExit(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            TxtD5.text = "";
            ZoneD5 = false;
        }
    }

}
