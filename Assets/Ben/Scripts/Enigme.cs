﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enigme : MonoBehaviour
{
    private Text TxtEnigme;
    // Start is called before the first frame update
    void Start()
    {
        TxtEnigme = GameObject.Find("TextEnigme").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            TxtEnigme.text = "Quelle est la bonne porte, parmi les portes devant toi ? Validez votre choix en appyant sur E proche de la porte";
        }
    }

    private void OnTriggerExit(Collider ply)
    {
        if (ply.gameObject.tag == "Player")
        {
            TxtEnigme.text = "";
        }
    }
}
