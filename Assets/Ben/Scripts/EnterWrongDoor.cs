﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnterWrongDoor : MonoBehaviour
{

    private Text TxtD1;
    private Text TxtD2;
    private Text TxtD3;
    private Text TxtD4;
    private Text TxtD6;
    private bool ZoneD1 = false;
    private bool ZoneD2 = false;
    private bool ZoneD3 = false;
    private bool ZoneD4 = false;
    private bool ZoneD6 = false;
    // Start is called before the first frame update
    void Start()
    {
        TxtD1 = GameObject.Find("TextD1").GetComponent<Text>();
        TxtD2 = GameObject.Find("TextD2").GetComponent<Text>();
        TxtD3 = GameObject.Find("TextD3").GetComponent<Text>();
        TxtD4 = GameObject.Find("TextD4").GetComponent<Text>();
        TxtD6 = GameObject.Find("TextD6").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && ZoneD1) {
            SceneManager.LoadScene("scene_1");
        }

        if(Input.GetKeyDown(KeyCode.E) && ZoneD2) {
            SceneManager.LoadScene("scene_1");
        }

        if(Input.GetKeyDown(KeyCode.E) && ZoneD3) {
            SceneManager.LoadScene("scene_1");
        }

        if(Input.GetKeyDown(KeyCode.E) && ZoneD4) {
            SceneManager.LoadScene("scene_1");
        }

        if(Input.GetKeyDown(KeyCode.E) && ZoneD6) {
            SceneManager.LoadScene("scene_1");
        }
    }

    private void OnTriggerEnter(Collider ply)
    {
        if (ply.gameObject.tag == "Porte1")
        {
            TxtD1.text = "Toutes les portes à droite";
            ZoneD1 = true;
        }
        if (ply.gameObject.tag == "Porte2")
        {
            TxtD2.text = "Aucune des portes à droite";
            ZoneD2 = true;
        }
        if (ply.gameObject.tag == "Porte3")
        {
            TxtD3.text = "Toutes les portes à gauche";
            ZoneD3 = true;
        }
        if (ply.gameObject.tag == "Porte4")
        {
            TxtD4.text = "Une des portes à gauche";
            ZoneD4 = true;
        }
        if (ply.gameObject.tag == "Porte6")
        {
            TxtD6.text = "Aucune des réponses à gauche";
            ZoneD6 = true;
        }
    }

    private void OnTriggerExit(Collider ply)
    {
        if (ply.gameObject.tag == "Porte1")
        {
            TxtD1.text = "";
            ZoneD1 = false;
        }
        if (ply.gameObject.tag == "Porte2")
        {
            TxtD2.text = "";
            ZoneD2 = false;
        }
        if (ply.gameObject.tag == "Porte3")
        {
            TxtD3.text = "";
            ZoneD3 = false;
        }
        if (ply.gameObject.tag == "Porte4")
        {
            TxtD4.text = "";
            ZoneD4 = false;
        }
        if (ply.gameObject.tag == "Porte6")
        {
            TxtD6.text = "";
            ZoneD6 = false;
        }
    }
}
